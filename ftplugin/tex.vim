let s:tw_pid = '/tmp/vim-texwatch/' . getpid() . '/pid'
let g:tex_alt_math_delim = 0

nnoremap <silent> <Leader>ia :call InsertAlgorithm() <CR>
nnoremap <silent> <Leader>ib :call InsertBMatrix() <CR>
nnoremap <silent> <Leader>ie :call InsertEquation() <CR>
nnoremap <silent> <Leader>if :call InsertFigure() <CR>
nnoremap <silent> <Leader>it :call InsertTikzFigure() <CR>
nnoremap <silent> <Leader>ip :call InsertFrame() <CR>
nnoremap <silent> <Leader>m  :call LaunchTexwatch() <CR>
nnoremap <silent> <Leader>n  :call ResetTexwatch() <CR>
nnoremap <silent> <Leader>ta :call ToggleAltMathDelim() <CR>

setlocal foldmethod=indent

autocmd VimLeave * silent! call KillTexwatch()

function ToggleAltMathDelim()
    let g:tex_alt_math_delim = g:tex_alt_math_delim == 1 ? 0 : 1
    if g:tex_alt_math_delim
        inoremap <expr>$ InlineMathDelim()
        inoremap $$ $
        call EchoMsg('Using alternate math delimiters')
    else
        iunmap $
        iunmap $$
        call EchoMsg('Using default math delimiters')
    endif
endfunction

function LaunchTexwatch()
    if !filereadable(s:tw_pid)
        let l:texwatch_pid = trim(system('make watch >/dev/null 2>&1 & echo $!'))
        let l:tw_dir = fnamemodify(s:tw_pid, ':p:h')
        if !isdirectory(l:tw_dir)
            call mkdir(l:tw_dir, 'p')
        endif
        call writefile([l:texwatch_pid], s:tw_pid)
        call EchoMsg('Launched process with pid ' . l:texwatch_pid)
    else
        let l:texwatch_pid = get(readfile(s:tw_pid), 0)
        call EchoError('Watch already running with pid ' . l:texwatch_pid)
    endif
endfunction

function KillTexwatch()
    if !filereadable(s:tw_pid)
        return
    endif
    let l:texwatch_pid = get(readfile(s:tw_pid), 0)
    let l:vim_texwatch_pid_dir = fnamemodify(s:tw_pid, ':p:h')
    let l:vim_texwatch_dir = fnamemodify(s:tw_pid, ':p:h:h')
    call system('kill ' . l:texwatch_pid . '; rm -rf ' . l:vim_texwatch_pid_dir .
               \'; (( $(ls ' . l:vim_texwatch_dir . ' | wc -l ) )) || rm -rf '. l:vim_texwatch_dir)
endfunction

function ResetTexwatch()
    call KillTexwatch()
    silent execute('normal! make clean')
    call LaunchTexwatch()
endfunction

function GetNumOpenTexBuffers()
    let l:typelist = GetBufferTypes()
    let num = 0
    for l:type in l:typelist
        if l:type == 'tex'
            let num += 1
        endif
    endfor
    return num
endfunction

function InlineMathDelim()
    let l:save = winsaveview()
    let l:ccol = col('.')

    let l:bline = search('\\(', 'cbW')
    let l:bcol = col('.')
    call winrestview(l:save)
    let l:eline = search('\\)', 'cbW')
    let l:ecol = col('.')

    if l:bline == 0
        return '\('
    elseif l:eline == 0
        return '\)'
    endif

    if l:bline > l:eline
        return '\)'
    elseif l:bline < l:eline
        return '\('
    else
        if l:bcol < l:ecol
            return '\('
        else
            return '\)'
        endif
    endif
endfunction

function InsertFigure()
    let l:save = winsaveview()
    let l:label = input('Figure label: ')

    if len(l:label) == 0
        call EchoError('No label specified')
        call winrestview(l:save)
        return
    endif

    let l:text = ["\\begin{figure}", "\t\\centering", "\t\t", "\t\\caption{TODO}", "\t\\label{fig:" . l:label . "}", "\\end{figure}"]
    call InsertTexScope(l:text, 2, 3)
endfunction

function InsertTikzFigure()
    let l:save = winsaveview()
    let l:label = input('Figure label: ')

    if len(l:label) == 0
        call EchoError('No label specified')
        call winrestview(l:save)
        return
    endif

    let l:text = ["\\begin{figure}", "\t\\centering", "\t\\begin{tikzpicture}", "\t\t", "\t\\end{tikzpicture}", "\t\\caption{TODO}", "\t\\label{fig:" . l:label . "}", "\\end{figure}"]
    call InsertTexScope(l:text, 3, 4)
endfunction

function InsertAlgorithm()
    let l:save = winsaveview()
    let l:label = input('Algorithm label: ')

    if len(l:label) == 0
        call EchoError('No label specified')
        call winrestview(l:save)
        return
    endif

    let l:text = ["\\begin{algorithm}", "\t\\SetAlgoLined", "\t\\KwResult{TODO}", "\t\t", "\t\\caption{TODO}", "\t\\label{alg:" . l:label . "}", "\\end{algorithm}"]
    call InsertTexScope(l:text, 3, 4)
endfunction

function InsertEquation()
    let l:save = winsaveview()
    let l:label = input('Equation label: ')

    if len(l:label) == 0
        call EchoError('No label specified')
        call winrestview(l:save)
        return
    endif

    let l:text = ["%", "\\begin{equation}", "\t", "\t\\label{eq:" . l:label ."}", "\\end{equation}", "%"]
    call InsertTexScope(l:text, 2, 3)
endfunction

function InsertBMatrix()
    let l:text = ["\\begin{bmatrix}", "\t", "\\end{bmatrix}"]
    call InsertTexScope(l:text,1,2)
endfunction

function InsertFrame()
    let l:text = ["\\begin{frame}", "\t", "\\end{frame}"]
    call InsertTexScope(l:text,1,2)
endfunction

function InsertTexScope(text, repldist, appenddist)
    let l:lnum = line('.')
    if LineIsEmpty(getline('.'))
        call setline(l:lnum, get(a:text, 0))
        call append(l:lnum, Tail(a:text))
        call cursor(l:lnum + a:repldist, strlen(getline(l:lnum + a:repldist)))
    else
        call append(l:lnum, a:text)
        call cursor(l:lnum + a:appenddist, strlen(getline(l:lnum + a:appenddist)))
    endif
    silent execute(':normal! zz')
endfunction
