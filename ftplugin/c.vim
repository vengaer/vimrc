nnoremap <silent> <Leader>m :silent make \| redraw!  <CR>
nnoremap <silent> <Leader>g :call HeaderGuard() <CR>
nnoremap <silent> <Leader>i :call IncludeHeader() <CR>

setlocal matchpairs-=<:>

function IsStandardCHeader(file)
    let l:c_headers = ['assert.h', 'complex.h', 'ctype.h', 'errno.h', 'fenv.h', 'float.h',
                      \'inttypes.h', 'iso646.h', 'limits.h', 'locale.h', 'math.h', 'setjmp.h',
                      \'signal.h', 'stdalign.h', 'stdarg.h', 'stdatomic.h', 'stdbool.h',
                      \'stddef.h', 'stdint.h', 'stdio.h', 'stdlib.h', 'stdnoreturn.h', 'string.h',
                      \'tgmath.h', 'threads.h', 'time.h', 'uchar.h', 'wchar.h', 'wctye.h']

    return index(l:c_headers, a:file) != -1
endfunction

function GetGccIncludePath(lang)
    return systemlist('gcc -v -x ' . a:lang . ' -E /dev/null 2>&1 1>/dev/null |' .
                     \'awk "/#include/,/End of search list./" |' .
                     \'grep -oP "\s*\K/.+" | paste -s | tr "\t|\n" " "')
endfunction

function IsGlobalCHeader(file)
    return FileExistsInPath(a:file, GetGccIncludePath('c'))
endfunction

function IsGlobalCxxHeader(file)
    return FileExistsInPath(a:file, GetGccIncludePath('c++'))
endfunction

function IncludeListSorter(file0, file1)
    let l:localpat = '#include ".*"'
    let l:cpat = '#include <.*\.h>'
    let l:f0name = substitute(a:file0, '#include <\(.\+\)>', '\1', 'g')
    let l:f1name = substitute(a:file1, '#include <\(.\+\)>', '\1', 'g')
    " Local include
    if match(a:file0, l:localpat) != -1
        " Local include
        if match(a:file1, l:localpat) != -1
            " Local includes sorted alphabetically
            return CompareAlphabetically(a:file0, a:file1)
        else
            " Local headers at the top
            return -1
        endif
    " C++ header
    elseif match(a:file0, l:cpat) == -1
        " Local include
        if match(a:file1, l:localpat) != -1
            " C++ headers below local includes
            return 1
        " C++ header
        elseif match(a:file1, l:cpat) == -1
            " C++ headers sorted alphabetically
            return CompareAlphabetically(a:file0, a:file1)
        else
            " C++ headers above C and API
            return -1
        endif
    elseif IsStandardCHeader(l:f0name)
        " Local or C++ header
        if match(a:file1, l:cpat) == -1
            " C headers below local and C++ headers
            return 1
        elseif IsStandardCHeader(l:f1name)
            " C headers sorted alphabetically
            return CompareAlphabetically(a:file0, a:file1)
        else
            " C headers above API
            return -1
        endif
    else "API
        if match(a:file1, l:cpat) == -1 || IsStandardCHeader(l:f1name)
            " API at the bottom
            return 1
        else
            " API headers sorted alphabetically
            return CompareAlphabetically(a:file0, a:file1)
        endif
    endif
endfunction

function GetIncludeType(include)
    let l:localpat = '#include ".\+"'
    let l:cxxpat = '#include <.\+[^.h]>'
    let l:filename = substitute(a:include, '#include <\(.\+\)>', '\1', 'g')

    if match(a:include, l:localpat) != -1
        return 0 " Local
    elseif match(a:include, l:cxxpat) != -1
        return 1 " C++
    elseif IsStandardCHeader(l:filename)
        return 2 " C
    endif
    return 3 " API
endfunction

function IncludeListWithEmptyLines(includelist)
    let list = []

    let c0 = GetIncludeType(get(a:includelist, 0))
    let c1 = c0
    for l:file in a:includelist
        let c0 = GetIncludeType(l:file)
        if c0 != c1
            call add(list, '')
            let c1 = c0
        endif

        call add(list, l:file)
    endfor

    return list

endfunction

function BufferIsHeaderFile()
    let l:exts = ['h', 'hpp']
    let l:extension = expand('%:e')
    return index(l:exts, l:extension) != -1
endfunction

function GetHeaderGuardLine()
    return search('\s*#ifndef\s*\([A-Z]\+[._]*[A-Z]*\)\s*$\n*\s*#define\s*\1\s*$')
endfunction

function FormatIncludeDirective(filename)
    if IsGlobalCHeader(a:filename) || IsGlobalCxxHeader(a:filename)
        return '#include <' . a:filename . '>'
    endif

    return '#include "' . a:filename . '"'
endfunction

function InsertBlockScope(type)
    let l:save = winsaveview()
    let l:name = input('Name of ' . a:type . ': ')

    if len(l:name) == 0
        call EchoError('No name specified')
        call winrestview(l:save)
        return
    endif

    let l:startline = a:type . ' ' . l:name . ' {'
    let l:endline = '} /* ' . a:type . ' ' . l:name . ' */'

    let l:lnum = line('.')
    if LineIsEmpty(getline('.'))
        " Replace current line rather than appending
        call setline(l:lnum, startline)
        call append(l:lnum, ['', endline])
        call cursor(l:lnum + 1, 0)
    else
        " Append after current line
        call append(l:lnum, [startline, '', endline])
        call cursor(l:lnum + 2, 0)
    endif

    call EchoMsg('Added ' . a:type . ' ' . l:name)
endfunction

function HeaderGuard()
    let l:save = winsaveview()
    let l:filename = expand('%:t')

    if !BufferIsHeaderFile()
        call EchoError(l:filename . ' is not a header file')
        call winrestview(l:save)
        return
    endif

    let l:lnum = GetHeaderGuardLine()
    if l:lnum
        call EchoError('Header guard already present on line' . l:lnum)
        call winrestview(l:save)
        return
    endif

    let l:guard_name = substitute(toupper(filename), '\.', '_', 'g')

    let l:ifndef = '#ifndef ' . l:guard_name
    let l:define = '#define ' . l:guard_name
    let l:endif  = '#endif /* ' . l:guard_name . ' */'

    call append('^', [l:ifndef, l:define, ''])

    call append('$', ['', l:endif])

    call EchoMsg('Header guard added')
endfunction

function IncludeHeader()
    let l:save = winsaveview()
    let l:includefile = input('File to include: ')

    if len(l:includefile) == 0
        call EchoError('No file provided')
        call winrestview(l:save)
        return
    endif

    let l:lnum = search('\s*#include\s*\(<\|"\)\s*' . l:includefile . '\s*\(>\|"\)')

    if l:lnum != 0
        call EchoError('File ' . l:includefile . ' already included on line ' . l:lnum)
        call winrestview(l:save)
        return
    endif

    let l:search_result = execute(':g/#include')

    if match(l:search_result, 'Pattern not found') != -1
        if BufferIsHeaderFile()
            let l:hglnum = GetHeaderGuardLine()
            if l:hglnum == 0
                call setline(1, FormatIncludeDirective(l:includefile))
            else
                call append(l:hglnum + 1, ['', FormatIncludeDirective(l:includefile)])
            endif
        else
            call setline(1, FormatIncludeDirective(l:includefile))
        endif
    else
        let includes = split(l:search_result, '\n')
        let l:include_start = search(get(includes, 0))
        let l:include_end = search(get(includes, len(l:includes) - 1))

        if !LinesInRangeMatchPattern(l:include_start, l:include_end, '^#include \(<\|"\).\+\(>\|"\)$\|^\s*$')
            call EchoError('Non-include lines among include directives')
            call winrestview(l:save)
            return
        endif

        call add(includes, FormatIncludeDirective(l:includefile))

        call sort(includes, "IncludeListSorter")
        let includes = IncludeListWithEmptyLines(includes)

        let l:dellines = l:include_end - l:include_start

        if l:include_start == l:include_end
            call setline(l:include_start, get(includes, 0))
            call append(l:include_start, Tail(includes))
        else
            silent execute('normal! ' . l:include_start . 'gg' . l:dellines . 'dj')
            call append(l:include_start - 1, includes)
        endif
    endif

    call EchoMsg('Included file ' . includefile)
    let l:save.lnum += 1
    call winrestview(l:save)
endfunction
