" {{{ Editor -- Runtimes

runtime! archlinux.vim
runtime  macros/matchit.vim
runtime  plugin/undo.vim
runtime  plugin/rc.vim

" }}}

" {{{ Editor -- Leader

let mapleader = ' '

" }}}

" {{{ Editor -- Basics

colo desert
filetype plugin on
syntax on

" Disable netrw
let loaded_netrwPlugin = 1

set incsearch

" No status bar
set laststatus=0

" Show current mode
set showmode

" Faster redraw
set ttyfast

" Shorter timeout for key mappings
set timeout timeoutlen=500 ttimeoutlen=50

" Abbreviate/truncate messages and disable :intro
set shortmess+=aTI

" No highlight for vertical split
highlight VertSplit ctermfg=0

" Hide buffers rather than closing them
set hidden

" Sane backspace
set backspace=2

" Don't jump to matching char upon insertion
set noshowmatch

" Disable modeline
set nomodeline

" }}}

" {{{ Editor -- Tabs

set expandtab
set tabstop=4
set shiftwidth=4

" }}}

" {{{ Editor -- Commands

" List mappings for filetype
command! -nargs=? -bar Mapping call ListMappings(PreprocFiletype(<q-args>))

" Generate tags
command! -nargs=0 -bar MakeTags :call system('ctags -R .')

" }}}

" {{{ Editor -- Folding

set foldenable
set foldnestmax=3

" Fold vimrc, nothing else
if IsMyVimrc(expand('%:p'))
    set foldlevelstart=0
    set foldmethod=marker
else
    set foldlevelstart=99
    set foldmethod=syntax
endif

" }}}

" {{{ Editor -- Formatting

" Delete comment character when joining lines
set formatoptions+=j

" Attempt to keep words intact when wrapping lines
set linebreak

" Round indent to multiple of shiftwidth
set shiftround

" }}}

" {{{ Editor -- History

" Set persistent undo file
call SetPersistentUndo()

set history=10000
set undolevels=1000

" }}}

" {{{ Editor -- Find

" Search subdirs
set path+=**

" }}}

" {{{ Command -- Mappings

" Abbreviate fzf :Files to f
cnoreabbrev <expr> f ((getcmdtype() is# ':' && getcmdline() is# 'f') ? ('find') : ('f'))

" fzf Files
nnoremap <silent> <Leader>f :Files <CR>

" fzf :Ag
nnoremap <silent> <Leader>a :Ag <CR>

" fzf :Rg
nnoremap <silent> <Leader>r :Rg <CR>

" fzf :Marks
nnoremap <silent> <Leader>m :Marks <CR>

" Save files opened as non-root
cnoremap w!! execute 'w !sudo tee >/dev/null %' <CR> <Bar> edit!

" }}}

" {{{ Ex Mode

" Disable Ex mode
nnoremap Q <Nop>

" }}}

" {{{ Normal -- Find & Replace

" Replace word under cursor
nnoremap <Leader>z :%s/\<<C-r><C-w>\>/

" }}}

" {{{ Normal -- Folding

" Fold/unfold
nnoremap <Leader>å za
" Fold/unfold recursively
nnoremap <Leader>Å zA

" Fold all
nnoremap <Leader>ö zm
" Fold all recursively
nnoremap <Leader>Ö zM

" Unfold all
nnoremap <Leader>ä zr
" Unfold all recursively
nnoremap <Leader>Ä zR

" }}}

" {{{ Normal -- Formatting

nnoremap <silent> <Leader>w :call StripTrailingWhitespace() <CR>
" }}}

" {{{ Normal -- Jumping

" Jump to matching char using tab
nmap <Tab> %

" Search matches in the center of the window
nnoremap n nzzzv
nnoremap N Nzzzv

" Cursor in center when jumping
nnoremap g; g;zz
nnoremap g, g,zz

" }}}

" {{{ Normal -- Line Navigation

" Swap j/k and gj/gk
nnoremap j gj
nnoremap k gk
nnoremap gj j
nnoremap gk k

" }}}

" {{{ Normal -- List & Open Buffer

" List and open/switch to file/buffer
nnoremap <Leader>b :ls<CR>:b<Space>
nnoremap <Leader>e :call ListDirContents()<CR>:e<Space>

" }}}

" "{{{ Normal -- Navigate Buffers

nnoremap <silent> <Leader>l :bnext <CR>
nnoremap <silent> <Leader>h :bprev <CR>
nnoremap <silent> <Leader>d :bd <CR>

" }}}

" {{{ Normal -- Navigate Splits

" Navigate splits
nnoremap <silent> <C-H> <C-W>h
nnoremap <silent> <C-J> <C-W>j
nnoremap <silent> <C-K> <C-W>k
nnoremap <silent> <C-L> <C-W>l

" Move splits
nnoremap <silent> <Leader><C-H> <C-W>H
nnoremap <silent> <Leader><C-J> <C-W>J
nnoremap <silent> <Leader><C-K> <C-W>K
nnoremap <silent> <Leader><C-L> <C-W>L

" }}}

" {{{ Normal -- Quickfix

" Jump quickfix history
nnoremap <silent> <Leader>p :cnewer <CR>
nnoremap <silent> <Leader>o :colder <CR>
" Open quickfix window
nnoremap <silent> <Leader>_ :botright vert copen 40 <CR>
" Close quickfix window
nnoremap <silent> <Leader>- :cclose <CR>

" }}}

" {{{ Normal -- Redraw

nnoremap <silent> <Leader>u :redraw! <CR>

" }}}

" {{{ Normal -- Scratch Buffer

nnoremap <silent> <Leader>c :call CloseVisibleScratchBuffers() <CR>

" }}}

" {{{ Normal -- Spelling

" Spell aliases
nnoremap <Leader>sn ]s
nnoremap <Leader>sp [s
nnoremap <Leader>sg zg
nnoremap <Leader>sG zG
nnoremap <Leader>sö z=

" }}}

" {{{ Normal -- Tags

" Generate tags
nnoremap <Leader>, :MakeTags<CR>
" Jump down the tag stack
nnoremap <Leader>j   <C-]>
" Disambiguate tag
nnoremap <Leader>gj g<C-]>
" Jump back up the tag stack
nnoremap <Leader>k   <C-t>

" }}}

" {{{ Normal -- Toggle

" Toggle search highlighting
nnoremap <silent> <Leader>th :set hlsearch!<CR>
" Toggle ignore case
nnoremap <silent> <Leader>tc :set ignorecase! <CR>
" Toggle spell
nnoremap <silent> <Leader>ts :set spell! <CR>
" Toggle line numbers
nnoremap <silent> <Leader>tn :set number! <CR>
" Toggle relative line numbers
nnoremap <silent> <Leader>tr :set relativenumber! <CR>
" Toggle cursor line
nnoremap <silent> <Leader>tl :set cursorline! <CR>
" Toggle incsearch
nnoremap <silent> <Leader>ti :set incsearch! <CR>
" Toggle folding
nnoremap <silent> <Leader>tf :set foldenable! <CR>

" }}}

" {{{ Normal -- Umlauts

nnoremap å `
nnoremap ä :
nnoremap ö ;
nnoremap gö g;zz

" }}}

" {{{ Normal -- Vimrc

" Source vimrc
nnoremap <silent> <Leader>vs :source $MYVIMRC <CR>
" Edit vimrc
nnoremap <silent> <Leader>ve :e $MYVIMRC <CR>

" }}}

" {{{ Normal -- Yanking & Selection

" Yank to end of line
nnoremap Y y$

" Reselect last inserted/pasted text
nnoremap <Leader>y `[V`]

" }}}

" {{{ Operator Pending -- Text Object Aliases

" Aliases for text objects with (, { and [
" v -- alias for (
" b -- alias for {
" r -- alias for [
onoremap ab :normal va{<CR>
onoremap ar :normal va[<CR>
onoremap av :normal va(<CR>
onoremap ib :normal vi{<CR>
onoremap ir :normal vi[<CR>
onoremap iv :normal vi(<CR>

" }}}

" {{{ Visual -- Folding

" Fold/unfold
xnoremap <Leader>å za
" Fold/unfold recusively
xnoremap <Leader>Å zA

" Fold all
xnoremap <Leader>ö zm
" Fold all recursively
xnoremap <Leader>Ö zM

" Unfold all
xnoremap <Leader>ä zr
" Unfold all recursively
xnoremap <Leader>Ä zR

" }}}

" {{{ Visual -- Jumping

" Jump to matching char using tab
xmap <Tab> %

" }}}

" {{{ Visual -- Text Object Aliases

xnoremap ab a{
xnoremap ar a[
xnoremap av a(
xnoremap ib i{
xnoremap ir i[
xnoremap iv i(

" }}}

" {{{ Visual -- Umlauts --

xnoremap å `
xnoremap ä :
xnoremap ö ;

" }}}

" {{{ Autocmds -- BufWrite

augroup g_ev
autocmd!
autocmd BufWritePre * silent! call StripTrailingWhitespaceExcludingFiletypes(['markdown'])
augroup end

" }}}

" {{{ Autocmds -- Filetype

augroup g_filetype
autocmd!
autocmd BufRead,BufNewFile *.h,*.c set filetype=c
autocmd BufRead,BufNewFile *.S,*.s set filetype=asm
autocmd BufRead,BufNewFile Jenkinsfile set filetype=groovy
autocmd BufRead,BufNewFile master.cfg set filetype=python
augroup end

" }}}

" {{{ Autocmds -- Formatting

" Don't insert comment on o or O
augroup g_fmt
autocmd!
autocmd FileType * setlocal formatoptions-=o
augroup end

" }}}

" {{{ Autocmds -- Quickfix

" Open quickfix/location lists automatically on make etc.
augroup g_qfix
autocmd!
autocmd QuickFixCmdPost [^l]* nested botright vert cwindow 40
autocmd QuickFixCmdPost    l* nested botright vert lwindow 40
augroup end

" }}}

" {{{ Filetype -- Tex

let g:tex_flavor = "latex"

" }}}
