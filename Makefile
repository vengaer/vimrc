vimdir     := $(HOME)/.vim
root       := $(abspath $(CURDIR))
rc         := $(HOME)/.vimrc
dirs       := $(addprefix $(vimdir)/,plugin ftplugin after)
fzfdir     := $(vimdir)/pack/fzf/start

MKDIR      := mkdir
COMMAND    := command
TOUCH      := touch
LN         := ln
RM         := rm

deps       := ctags fzf ag rg

builddir   := build
deps.stamp := $(patsubst %,$(builddir)/.%.stamp,$(deps))
fzf.stamp  := $(builddir)/.fzf.vim.stamp
fzf        := $(fzfdir)/fzf.vim

.PHONY: all
all: $(dirs) $(rc)

$(rc): $(deps.stamp) $(fzf)
	$(LN) -sf $(root)/$(patsubst .%,%,$(notdir $@)) $@

$(deps.stamp): | $(builddir)
	$(COMMAND) -v $(patsubst $(builddir)/.%.stamp,%,$@) >/dev/null
	$(TOUCH) $@

$(fzf): $(fzf.stamp) | $(fzfdir)
	$(LN) -sfn $(root)/$(notdir $@) $(fzf)

$(fzf.stamp):
	git submodule update --init
	$(TOUCH) $@

$(builddir) $(vimdir) $(fzfdir):
	$(MKDIR) -p $@

$(dirs): | $(vimdir)
	$(LN) -sfn $(root)/$(notdir $@) $@

.PHONY: uninstall
uninstall:
	$(RM) $(dirs) $(rc) $(fzf)

.PHONY: clean
clean:
	$(RM) -rf $(builddir)

$(VERBOSE).SILENT:
