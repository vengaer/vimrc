function PreprocFiletype(type)
    let type = tolower(a:type)
    if match(type, '^c\(++\|c\)$') != -1
        let type = 'cpp'
    elseif match(type, '^s$') != -1
        let type = 'asm'
    endif
    return type
endfunction

function ListMappings(type)
    if a:type != ''
        let type = a:type
    else
        let type = &filetype
    endif

    let l:ftplugin = $HOME . '/.vim/ftplugin/' . type . '.vim'
    let l:after_ftplugin = $HOME . '/.vim/after/ftplugin/' . type . '.vim'

    let type = substitute(type, '\v(.)(.*)', '\u\1\L\2', 'g')

    let l:contents = []

    if filereadable(l:ftplugin)
        let l:contents += readfile(l:ftplugin)
    endif
    if filereadable(l:after_ftplugin)
        let l:contents += readfile(l:after_ftplugin)
    endif

    let l:contents = ExpandFtRuntimess(l:contents)
    let l:mappings = FilterList(l:contents, '\v^[a-z](nore)?map')

    if a:type == &filetype
        let l:mappings = l:mappings + GetConditionalMappings(l:contents)
    endif

    if len(l:mappings) == 0
        call EchoMsg('No mappings for ' . type)
        return
    endif

    echo type . ' mappings'
    echo repeat('=', strlen(type) + 9)

    let l:mappings = SubstituteListItems(l:mappings, '\v(\<[sS][iI][lL][eE][nN][tT]\>|\<[cC][rR]\>)', '', 'g')

    let l:mappings = sort(l:mappings)

    let l:global  = GetGlobalMappings(l:mappings)
    let l:command = GetCommandMappings(l:mappings)
    let l:insert  = GetInsertMappings(l:mappings)
    let l:normal  = GetNormalMappings(l:mappings)
    let l:pending = GetOperatorPendingMappings(l:mappings)
    let l:term    = GetTermMappings(l:mappings)
    let l:select  = GetSelectMappings(l:mappings)
    let l:visual  = GetVisualMappings(l:mappings)
    let l:vissel  = GetVisualSelectMappings(l:mappings)

    call PrintMappingList('Global', l:global)
    call PrintMappingList('Command', l:command)
    call PrintMappingList('Insert', l:insert)
    call PrintMappingList('Normal', l:normal)
    call PrintMappingList('Operator Pending', l:pending)
    call PrintMappingList('Terminal', l:term)
    call PrintMappingList('Select', l:select)
    call PrintMappingList('Visual', l:visual)
    call PrintMappingList('Visual + Select', l:vissel)
endfunction

function ExpandFtRuntimess(list)
    let result = []

    for l:item in a:list
        if match(l:item, '^runtime\s*ftplugin/.\+') != -1
            let l:file = substitute(l:item, '^runtime\s*', '', 'g')
            let result = result +  readfile($HOME . '/.vim/' . l:file)
        else
            call add(result, l:item)
        endif
    endfor

    return result
endfunction

function PrintMappingList(mode, list)
    if len(a:list) == 0
        return
    endif
    echom a:mode . ':'
    for l:item in a:list
        if match(l:item, '\v[a-z](nore)?map.+:.+') != -1
            " Format output
            let l:lhs = substitute(l:item, '\v[a-z](nore)?map\s*(.+):[^:]+$', '\2', 'g')
            let l:rhs = substitute(l:item, '\v[a-z](nore)?map\s*.+(:[^:]+)', '\2', 'g')
            echo repeat(' ', 4) . l:lhs . repeat(' ', 16 - strlen(l:lhs)) . l:rhs
        else
            echo repeat(' ', 4) . substitute(l:item, '\v[a-z](nore)?map\s*', '', 'g')
        endif
    endfor
endfunction

function GetConditionalMappings(list)
    let result = []
    let incond = 0
    let condtrue = 0

    for l:item in a:list
        if match(l:item, '\v^\s*(else)?if\s+.*g:[^ ]+.*') != -1
            let l:cond = substitute(l:item, '\v^\s*(else)?if\s+([^ ]*g:[^ ]+).*$', '\2', 'g')
            let condtrue = eval(l:cond)
            let incond = 1
            continue
        elseif match(l:item, '\v\s*else\s*$') != -1
            let incond = 2
            continue
        elseif match(l:item, '\v\s*endif\s.+') != -1
            let incond = 0
            continue
        endif

        if match(l:item, '\v^\s*[a-z](nore)?map') != -1

            if incond == 1 && condtrue
                call add(result, trim(l:item))
            elseif incond == 2 && !condtrue
                call add(result, trim(l:item))
            endif
        endif
    endfor

    return result
endfunction

function GetGlobalMappings(list)
    return GetItemsMatchingPattern(a:list, '\v^\s*(nore)?map')
endfunction

function GetCommandMappings(list)
    return GetItemsMatchingPattern(a:list, '\v^\s*c(nore)?map')
endfunction

function GetInsertMappings(list)
    return GetItemsMatchingPattern(a:list, '\v^\s*i(nore)?map')
endfunction

function GetNormalMappings(list)
    return GetItemsMatchingPattern(a:list, '\v^\s*n(nore)?map')
endfunction

function GetOperatorPendingMappings(list)
    return GetItemsMatchingPattern(a:list, '\v^\s*o(nore)?map')
endfunction

function GetTermMappings(list)
    return GetItemsMatchingPattern(a:list, '\v^\s*t(nore)?map')
endfunction

function GetSelectMappings(list)
    return GetItemsMatchingPattern(a:list, '\v^\s*s(nore)?map')
endfunction

function GetVisualMappings(list)
    return GetItemsMatchingPattern(a:list, '\v^\s*x(nore)?map')
endfunction

function GetVisualSelectMappings(list)
    return GetItemsMatchingPattern(a:list, '\v^\s*v(nore)?map')
endfunction


