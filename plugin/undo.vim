function SetPersistentUndo()
    if has('persistent_undo')
        set undofile
        set undodir=~/.vim/undo
        if !isdirectory(expand(&undodir))
            call mkdir(expand(&undodir), 'p')
        endif
    endif
endfunction
