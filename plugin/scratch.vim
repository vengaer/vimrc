function ShellCmdInVerticalScratchBuffer(command) abort
    let l:output = ['## ' . a:command . ' ##', repeat('=', 6 + strlen(a:command))  ] +
                   \systemlist(a:command)
    let l:maxwidth = winwidth(winnr()) / 2
    let l:maxstrlen = ListMaxStrlen(l:output)
    let l:width = l:maxstrlen > l:maxwidth ? l:maxwidth : l:maxstrlen
    call OpenVerticalScratchBuffer(l:output, l:width)
endfunction

function ShellCmdInHorizontalScratchBuffer(command) abort
    let l:output = ['## ' . a:command . ' ##', repeat('=', 6 + strlen(a:command))  ] +
                   \systemlist(a:command)
    let l:maxheight = winheight(winnr()) / 2
    let l:listlen = len(l:output)
    let l:height = l:listlen > l:maxheight ? l:maxheight : l:listlen
    call OpenHorizontalScratchBuffer(l:output, l:height)
endfunction

function OpenVerticalScratchBuffer(text, width)
    botright vnew
    silent execute('vertical resize ' . a:width)
    call MakeScratchBuffer(a:text)
endfunction

function OpenHorizontalScratchBuffer(text, height)
    botright new
    silent execute('resize ' . a:height)
    call MakeScratchBuffer(a:text)
endfunction

function MakeScratchBuffer(text)
    if !len(a:text)
        call EchoError('File content list is empty')
        silent execute('execute ' . winnr() . ' . "wincmd c"')
        return
    endif
    setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile
    call setline(1, get(a:text, 0))
    call append(1, Tail(a:text))
    setlocal nomodifiable
endfunction

function CloseVisibleScratchBuffers()
    let l:bufs = GetVisibleScratchBuffers()
    for l:winnr in reverse(l:bufs)
       silent execute('execute ' . l:winnr . ' . "wincmd c"')
    endfor
endfunction

function GetVisibleScratchBuffers()
    let scratchwins = []
    let l:winnrs = GetOpenWindows()
    for l:winnr in l:winnrs
        if getwinvar(l:winnr, '&buftype') == 'nofile' && getwinvar(l:winnr, '&bufhidden') == 'wipe'
            call add(scratchwins, l:winnr)
        endif
    endfor
    return scratchwins
endfunction

function GetOpenWindows()
    let l:winnrs = []
    silent execute('windo call add(l:winnrs, winnr())')
    return l:winnrs
endfunction
