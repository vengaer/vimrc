function LineIsEmpty(line)
    return match(a:line, '^\s*$') != -1
endfunction

function LinesInRangeMatchPattern(start, end, pattern)
    let l:ctr = a:start
    while l:ctr < a:end
        if match(getline(l:ctr), a:pattern) == -1
            return 0
        endif
        let l:ctr = l:ctr + 1
    endwhile

    return 1
endfunction

function FileExistsInPath(file, pathlist)
    for l:dir in a:pathlist
        let l:filepath = system(join(['find', l:dir, '-name', a:file]))
        if l:filepath != ""
            return 1
        endif
    endfor

    return 0
endfunction

function CompareAlphabetically(s0, s1)
    return a:s0 == a:s1 ? 0 : a:s0 > a:s1 ? 1 : -1
endfunction

